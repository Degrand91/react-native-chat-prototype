import { createStore, combineReducers, applyMiddleware, compose } from "redux"
import createSagaMiddleware from "redux-saga"
import { spawn } from "redux-saga/effects"
import { happinessReducer, happinessSaga } from "./features/account/store/"
const sagaMiddleware = createSagaMiddleware()

const rootReducer = combineReducers({
  happiness: happinessReducer
})

const enhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

function* rootSaga() {
  yield spawn(happinessSaga)
}

sagaMiddleware.run(rootSaga)

export default store
