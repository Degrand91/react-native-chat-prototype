import * as AccountActions from "./store/account.actions"
import { reducer as accountReducer } from "./store/account.reducer"
import { saga as accountSaga } from "./store/account.sagas"
import * as AccountSelectors from "./store/account.selectors"
import getAccountRoutes from "./Account.router"

// import SignIn from "./views/SignIn"
// import SignUp from "./views/SignUp"
// import ForgotPassword from "./views/ForgotPassword"
// import ResetPassword from "./views/ResetPassword"
// import ConfirmEmail from "./views/ConfirmEmail"
// import PremiumCheckout from "./views/PremiumCheckout"

// import ConfirmEmailBanner from "./containers/ConfirmEmailBanner"
// import AuthRedirect from "./containers/AuthRedirect"

// import withAuth from "./lib/withAuth"

export {
  AccountActions,
  accountReducer,
  accountSaga,
  AccountSelectors,
  getAccountRoutes,
  SignIn,
  SignUp,
  ForgotPassword,
  ResetPassword,
  PremiumCheckout,
  ConfirmEmail,
  ConfirmEmailBanner,
  AuthRedirect,
  withAuth
}
