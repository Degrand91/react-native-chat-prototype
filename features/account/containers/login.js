import { connect } from "react-redux"

import { signIn } from "../store/actions"
import { getIsAuthenticated, getIsLoading } from "../store/selectors"

import LoginForm from "../components/LoginForm"

const mapState = state => ({
  isAuthenticated: getIsAuthenticated(state),
  isLoading: getIsLoading(state)
})

const mapDispatch = dispatch => ({
  onSubmit: accountDetails => dispatch(signIn(accountDetails))
})

const Login = connect(
  mapState,
  mapDispatch
)(LoginForm)

export default Login
