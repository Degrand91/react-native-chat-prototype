export const types = {
  SIGN_IN: "[Account] Sign In",
  TOKEN_SIGN_IN: "[Account] Token Sign In",
  TOKEN_SIGN_IN_FAIL: "[Account] Token Sign In Fail",
  SIGN_IN_SUCCESS: "[Account] Sign In Success",
  SIGN_IN_FAIL: "[Account] Sign In Fail",
  SIGN_OUT: "[Account] Sign Out",
  SIGN_OUT_SUCCESS: "[Account] Sign Out Success",
  SIGN_OUT_FAIL: "[Account] Sign Out Fail",

  REGISTER: "[Account] Register",
  REGISTER_SUCCESS: "[Account] Register Success",
  REGISTER_FAIL: "[Account] Register Fail",

  FORGOT_PASSWORD: "[Account] Forgot Password",
  FORGOT_PASSWORD_SUCCESS: "[Account] Forgot Password Success",
  FORGOT_PASSWORD_FAIL: "[Account] Forgot Password Fail",

  RESET_PASSWORD: "[Account] Reset Password",
  RESET_PASSWORD_SUCCESS: "[Account] Reset Password Success",
  RESET_PASSWORD_FAIL: "[Account] Reset Password Fail",

  CONFIRM_EMAIL: "[Account] Confirm Email",
  CONFIRM_EMAIL_SUCCESS: "[Account] Confirm Email Success",
  CONFIRM_EMAIL_FAIL: "[Account] Confirm Email Fail",

  REQUEST_CONFIRMATION: "[Account] Request Confirmation Email",
  REQUEST_CONFIRMATION_SUCCESS: "[Account] Request Confirmation Success",
  REQUEST_CONFIRMATION_FAIL: "[Account] Request Confirmation Fail",

  GET_USER_DETAILS: "[Account] Get User details",
  GET_USER_DETAILS_SUCCESS: "[Account] Get User details Success",
  GET_USER_DETAILS_FAIL: "[Account] Get User details Fail",

  SET_PREVIOUS_ROUTE: "[Account] Set Previous Route"
}

export const signIn = ({ email, password }) => ({
  type: types.SIGN_IN,
  payload: { email, password }
})

export const tokenSignIn = () => ({ type: types.TOKEN_SIGN_IN })

export const signInSuccess = token => ({
  type: types.SIGN_IN_SUCCESS,
  payload: token
})

export const signInFail = errorCode => ({
  type: types.SIGN_IN_FAIL,
  payload: errorCode
})

export const tokenSignInFail = () => ({ type: types.TOKEN_SIGN_IN_FAIL })

export const signOut = () => ({ type: types.SIGN_OUT })

export const signOutSuccess = () => ({ type: types.SIGN_OUT_SUCCESS })

export const signOutFail = () => ({ type: types.SIGN_OUT_FAIL })

/** REGISTER */
export const register = ({
  firstName,
  lastName,
  email,
  password,
  allowMarketing
}) => ({
  type: types.REGISTER,
  payload: { firstName, lastName, email, password, allowMarketing }
})
export const registerSuccess = token => ({
  type: types.REGISTER_SUCCESS,
  payload: token
})
export const registerFail = errorCode => ({
  type: types.REGISTER_FAIL,
  payload: errorCode
})

/** FORGOT PASSWORD */
export const forgotPassword = email => ({
  type: types.FORGOT_PASSWORD,
  payload: email
})
export const forgotPasswordSuccess = () => ({
  type: types.FORGOT_PASSWORD_SUCCESS
})
export const forgotPasswordFail = () => ({
  type: types.FORGOT_PASSWORD_FAIL
})

/** RESET PASSWORD */
export const resetPassword = ({ resetToken, password }) => ({
  type: types.RESET_PASSWORD,
  payload: { password, token: resetToken }
})
export const resetPasswordSuccess = () => ({
  type: types.RESET_PASSWORD_SUCCESS
})
export const resetPasswordFail = () => ({
  type: types.RESET_PASSWORD_FAIL
})

/** CONFIRM EMAIL */
export const confirmEmail = confirmToken => ({
  type: types.CONFIRM_EMAIL,
  payload: confirmToken
})
export const confirmEmailSuccess = () => ({ type: types.CONFIRM_EMAIL_SUCCESS })
export const confirmEmailFail = () => ({ type: types.CONFIRM_EMAIL_FAIL })
export const requestConfirmation = () => ({
  type: types.REQUEST_CONFIRMATION
})
export const requestConfirmationSuccess = () => ({
  type: types.REQUEST_CONFIRMATION_SUCCESS
})
export const requestConfirmationFail = () => ({
  type: types.REQUEST_CONFIRMATION_FAIL
})

export const getUserDetails = token => ({
  type: types.GET_USER_DETAILS,
  payload: token
})

export const getUserDetailsSuccess = details => ({
  type: types.GET_USER_DETAILS_SUCCESS,
  payload: details
})

export const getUserDetailsFail = () => ({
  type: types.GET_USER_DETAILS_FAIL
})

export const setPreviousRoute = previousRoute => ({
  type: types.SET_PREVIOUS_ROUTE,
  payload: previousRoute
})

export const subscribe = token => ({
  type: types.SUBSCRIBE,
  payload: token
})

export const subscribeSuccess = () => ({
  type: types.SUBSCRIBE_SUCCESS
})

export const subscribeFail = () => ({
  type: types.SUBSCRIBE_FAIL
})

export const incrementSubscription = step => ({
  type: types.INCREMENT_SUBSCRIPTION_FLOW,
  payload: step
})
