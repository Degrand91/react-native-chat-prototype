export const initialState = {
  isAuthenticated: false,
  hasResetPassword: false,
  token: "",
  details: {},
  isLoading: false,
  previousRoute: "/",
  subscriptionStep: subscriptionSteps.get
}

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case types.SIGN_IN:
    case types.TOKEN_SIGN_IN:
    case types.SIGN_OUT:
    case types.REGISTER:
    case types.FORGOT_PASSWORD:
    case types.RESET_PASSWORD:
    case types.CONFIRM_EMAIL:
    case types.REQUEST_CONFIRMATION:
    case types.GET_USER_DETAILS:
      return { ...state, isLoading: true }

    case types.SIGN_IN_SUCCESS:
    case types.REGISTER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        token: action.payload
      }

    case types.SIGN_IN_FAIL:
    case types.TOKEN_SIGN_IN_FAIL:
    case types.REGISTER_FAIL:
    case types.FORGOT_PASSWORD_FAIL:
    case types.RESET_PASSWORD_FAIL:
    case types.CONFIRM_EMAIL_FAIL:
    case types.REQUEST_CONFIRMATION_FAIL:
    case types.GET_USER_DETAILS_FAIL:
      return { ...state, isLoading: false }

    case types.CONFIRM_EMAIL_SUCCESS:
    case types.REQUEST_CONFIRMATION_SUCCESS:
    case types.FORGOT_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false
      }

    case types.RESET_PASSWORD_SUCCESS:
      return {
        ...state,
        isLoading: false,
        hasResetPassword: true
      }

    case types.SIGN_OUT_SUCCESS:
      return {
        ...state,
        token: "",
        isAuthenticated: false,
        isLoading: false
      }

    case types.GET_USER_DETAILS_SUCCESS:
      return {
        ...state,
        details: action.payload,
        isLoading: false
      }

    case types.SET_PREVIOUS_ROUTE:
      return {
        ...state,
        previousRoute: action.payload
      }

    case types.SUBSCRIBE:
      return { ...state, isLoading: true }

    case types.SUBSCRIBE_SUCCESS:
      return { ...state, isLoading: false }

    case types.SUBSCRIBE_FAIL:
      return { ...state, isLoading: false }

    case types.INCREMENT_SUBSCRIPTION_FLOW:
      return { ...state, subscriptionStep: action.payload }

    default:
      return state
  }
}
