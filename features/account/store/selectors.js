import { createSelector } from "reselect"

const account = state => state.account

export const getIsAuthenticated = createSelector(
  account,
  state => state.isAuthenticated
)

export const getToken = createSelector(
  account,
  state => state.token || null
)

export const getIsLoading = createSelector(
  account,
  state => state.isLoading
)

export const getUserDetails = createSelector(
  account,
  state => state.details || null
)

export const getIsPremium = createSelector(
  getUserDetails,
  details => details.premium || null
)

export const getIsConfirmed = createSelector(
  getUserDetails,
  details => (details ? details.confirmed : false)
)

export const getHasResetPassword = createSelector(
  account,
  state => state.hasResetPassword
)

export const getPreviousRoute = createSelector(
  account,
  state => state.previousRoute
)

export const getSubscriptionStep = createSelector(
  account,
  state => state.subscriptionStep
)
