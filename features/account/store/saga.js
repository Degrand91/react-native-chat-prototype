import { takeLatest, put, call } from "redux-saga/effects"
import { decode } from "jsonwebtoken"

import * as Actions from "./account.actions"
import {
  getToken,
  saveToken,
  removeToken,
  signIn,
  signUp,
  requestPasswordReset,
  requestConfirmationEmail,
  confirmEmailAddress,
  resetPassword,
  getTokenString,
  getUserDetails,
  setAuthHeader,
  removeAuthHeader
} from "../services/account.service"
import { subscribe } from "../services/premium.service"

import subscriptionSteps from "../lib/subscriptionSteps"

export function* getTokenSaga(action, api = getToken) {
  const token = yield call(api)

  yield token
    ? put(Actions.signInSuccess(token))
    : put(Actions.tokenSignInFail(token))
}

export function* signInSaga(action, api = signIn, save = saveToken) {
  try {
    const {
      data: { result: token }
    } = yield call(api, action.payload)

    yield call(save, token)
    yield put(Actions.signInSuccess(decode(token)))
  } catch (err) {
    yield put(Actions.signInFail(err.response.data.code))
  }
}

export function* signInSuccessSaga(
  action,
  setHeader = setAuthHeader,
  getJwt = getTokenString
) {
  const token = yield call(getJwt)

  yield call(setHeader, token)
  yield put(Actions.getUserDetails())
}

export function* removeTokenSaga(
  action,
  api = removeToken,
  removeHeader = removeAuthHeader
) {
  try {
    yield call(api)
    yield call(removeHeader)
    yield put(Actions.signOutSuccess())
  } catch {
    yield put(Actions.signOutFail())
  }
}

export function* registerSaga(action, api = signUp, save = saveToken) {
  try {
    const {
      data: { result: token }
    } = yield call(api, action.payload)

    yield call(save, token)
    yield put(Actions.registerSuccess(decode(token)))
  } catch (err) {
    yield put(Actions.registerFail(err.response.data.code))
  }
}

export function* forgotPasswordSaga(action, api = requestPasswordReset) {
  try {
    yield call(api, action.payload)
    yield put(Actions.forgotPasswordSuccess())
  } catch (err) {
    yield put(Actions.forgotPasswordFail())
  }
}

export function* resetPasswordSaga(
  action,
  api = resetPassword,
  save = saveToken
) {
  try {
    const {
      data: { result: token }
    } = yield call(api, action.payload)

    yield call(save, token)
    yield put(Actions.resetPasswordSuccess())
  } catch {
    yield put(Actions.resetPasswordFail())
  }
}

export function* confirmEmailSaga(action, api = confirmEmailAddress) {
  try {
    const {
      data: { result }
    } = yield call(api, action.payload)

    yield result
      ? put(Actions.confirmEmailSuccess())
      : put(Actions.confirmEmailFail())
  } catch {
    yield put(Actions.confirmEmailFail())
  }
}

export function* requestConfirmationSaga(
  action,
  api = requestConfirmationEmail
) {
  try {
    yield call(api)

    yield put(Actions.requestConfirmationSuccess())
  } catch {
    yield put(Actions.requestConfirmationFail())
  }
}

export function* getUserDetailsSaga(action, api = getUserDetails) {
  try {
    const {
      data: { result }
    } = yield call(api, action.payload)

    yield result
      ? put(Actions.getUserDetailsSuccess(result))
      : put(Actions.getUserDetailsFail())
  } catch {
    yield put(Actions.getUserDetailsFail())
  }
}

export function* subscribeSaga(action, api = subscribe) {
  try {
    yield call(api, action.payload)
    yield put(Actions.subscribeSuccess())
    yield put(Actions.getUserDetails())
    yield put(Actions.incrementSubscription(subscriptionSteps.complete))
  } catch {
    yield put(Actions.subscribeFail())
  }
}

export function* saga() {
  yield takeLatest(Actions.types.TOKEN_SIGN_IN, getTokenSaga)
  yield takeLatest(Actions.types.SIGN_IN, signInSaga)
  yield takeLatest(
    [Actions.types.SIGN_IN_SUCCESS, Actions.types.REGISTER_SUCCESS],
    signInSuccessSaga
  )
  yield takeLatest(Actions.types.SIGN_OUT, removeTokenSaga)
  yield takeLatest(Actions.types.REGISTER, registerSaga)
  yield takeLatest(Actions.types.FORGOT_PASSWORD, forgotPasswordSaga)
  yield takeLatest(Actions.types.RESET_PASSWORD, resetPasswordSaga)
  yield takeLatest(Actions.types.CONFIRM_EMAIL, confirmEmailSaga)
  yield takeLatest(Actions.types.REQUEST_CONFIRMATION, requestConfirmationSaga)
  yield takeLatest(Actions.types.GET_USER_DETAILS, getUserDetailsSaga)
  yield takeLatest(Actions.types.SUBSCRIBE, subscribeSaga)
}
