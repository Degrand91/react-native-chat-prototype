import { connect } from "react-redux"

import { getIsAuthenticated } from "../store/account.selectors"

const mapState = state => ({
  isAuthenticated: getIsAuthenticated(state)
})

/**
 * A higher-order component which adds an isAuthenticated flag from the
 * store.
 * @param {*} component The component to add an authentication flag to.
 */
const withAuth = component => connect(mapState)(component)

export default withAuth
