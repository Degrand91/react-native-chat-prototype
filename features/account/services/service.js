import axios from "axios"
import Cookies from "js-cookie"
import { decode } from "jsonwebtoken"
import { pipe } from "@et/utils"

const DEFAULT_COOKIE_NAME = "_ewsid"
const ACCOUNT_API = `${CONFIG.api.url}${CONFIG.api.endpoints.userManager}`

/**
 * Removes the "JWT  " prefix from a base64 decoded JWT token string.
 * We don't know why this prefix is attached to the decoded string, but
 * we have to remove it so whatever I guess.
 * @param {string} tokenString A base64 decoded string.
 * @returns {string} The same string, but without the "JWT  " prefix.
 */
const extractToken = tokenString =>
  tokenString ? tokenString.replace(/JWT\s/, "").trim() : null

/**
 * Adds the "JWT  " prefix to a JWT token string.
 * @param {string} tokenString The encoded token to be prefixed.
 */
const insertPrefix = tokenString => `JWT  ${tokenString}`

/**
 * Decodes a base64 encoded string.
 * @param {string} encodedToken A base64 encoded string
 * @returns {string} A decoded string
 */
const decodeTokenString = encodedToken =>
  encodedToken ? atob(encodedToken) : null

/**
 * Base64 encodes a JWT token string.
 * @param {string} token The token to be encoded.
 * @returns {string} The encoded string.
 */
const encodeTokenString = token => (token ? btoa(token) : null)

/**
 * Checks for the presence of an authentication cookie to determine if a visitor is authenticated or
 * not.
 * @param {string} name The name of the cookie to check authentication against. Defaults to "_ewsid".
 * @param {*} storage A storage object to retrieve the string from. Defaults to the js-cookie library.
 * If a custom storage object is handed in, it *must* implement a get function.
 * @returns {boolean} true if an authentication cookie is present, false otherwise.
 */
export const isAuthenticated = (
  name = DEFAULT_COOKIE_NAME,
  storage = Cookies
) => !!storage.get(name)

/**
 * Retrieves a JWT token from a cookie.
 * @param {string} name The name of the cookie in which the token is stored as a value.
 * @param {*} storage A storage object to retrieve the string from. Defaults to the js-cookie library.
 * If a custom storage object is handed in, it *must* implement a get function (i.e. storage.get()).
 * @param {function} toJWT A callback function to convert the string into a JWT object.
 * @returns {object} The decoded JWT.
 */
export const getToken = (
  name = DEFAULT_COOKIE_NAME,
  storage = Cookies,
  toJWT = decode
) =>
  pipe(
    storage.get,
    decodeTokenString,
    extractToken,
    toJWT
  )(name)

/**
 * Retrieves a token in string format from a cookie.
 * @param {string} name The name of the cookie in which the token is stored as a value.
 * @param {*} storage A storage object to retrieve the string from. Defaults to the js-cookie library.
 * If a custom storage object is handed in, it *must* implement a get function (i.e. storage.get())
 */
export const getTokenString = (name = DEFAULT_COOKIE_NAME, storage = Cookies) =>
  pipe(
    storage.get,
    decodeTokenString,
    extractToken
  )(name)

/**
 * Saves a token to a cookie.
 * @param {string} token The token to save to a cookie.
 * @param {string} name The name of the cookie the token should be saved in.
 * @param {*} storage A storage object to save the token to. If a custom storage object is handed in,
 * it *must* implement a set function (i.e. storage.set())
 */
export const saveToken = (
  token,
  name = DEFAULT_COOKIE_NAME,
  storage = Cookies
) =>
  pipe(
    insertPrefix,
    encodeTokenString,
    encodedToken => storage.set(name, encodedToken)
  )(token)

/**
 * Removes the token for the currently authenticated user by deleting the cookie.
 * @param {string} name The name of the cookie in which the token is stored as a value.
 * @param {*} storage A storage object to retrieve the string from. Defaults to the js-cookie library.
 * If a custom storage object is handed in, it *must* implement a remove function (i.e. storage.remove()).
 */
export const removeToken = (name = DEFAULT_COOKIE_NAME, storage = Cookies) =>
  storage.remove(name)

/**
 * Sends a request to sign an account in.
 * @param {*} param0 Account details for the account being authenticated.
 * @param {string} url The URL of the API to send the request to.
 * @param {*} api The function used to make the request. Defaults to Axios.
 */
export const signIn = ({ email, password }, url = ACCOUNT_API, api = axios) =>
  api({
    method: "post",
    url: `${url}/signin`,
    data: { email, password }
  })

/**
 * Sends a reques to create a new account.
 * @param {*} param0 Account details for the new account to be created.
 * @param {*} url The URL of the API to send the request to.
 * @param {*} api The function used to make the request. Defaults to Axios.
 */
export const signUp = (
  { email, password, firstName, lastName, allowMarketing },
  url = ACCOUNT_API,
  api = axios
) =>
  api({
    method: "post",
    url: `${url}/signup`,
    data: { email, password, firstName, lastName, allowMarketing }
  })

/**
 * Sends a request to the backend requesting a password reset email.
 * @param {*} param0 THe email address for the password reset message to be sent to.
 * @param {*} url The URL of the API to send the request to.
 * @param {*} api The function used to make the request. Defaults to Axios.
 */
export const requestPasswordReset = (
  { email },
  url = ACCOUNT_API,
  api = axios
) =>
  api({
    method: "post",
    url: `${url}/forgot-password`,
    data: { email }
  })

/**
 * Sends a request to reset a user's password.
 * @param {*} newPassword The new password object. Contains a password string and token string.
 * @param {string} url THe URL of the API to send the request to.
 * @param {*} api The function used to make the request. Defaults to Axios.
 */
export const resetPassword = (newPassword, url = ACCOUNT_API, api = axios) =>
  api({
    method: "post",
    url: `${url}/reset-password`,
    data: newPassword
  })

/**
 * Sends a request to the backend to confirm a new user's email address.
 * @param {string} token The confirmation token included in the email sent to the user.
 * @param {string} url The URL of the API to send the request to.
 * @param {*} api The function used to make the request. Defaults to Axios.
 */
export const confirmEmailAddress = (token, url = ACCOUNT_API, api = axios) =>
  api({
    method: "post",
    url: `${url}/confirm-email/${token}`
  })

export const requestConfirmationEmail = (url = ACCOUNT_API, api = axios) =>
  api({
    method: "get",
    url: `${url}/confirm-email`
  })

export const getUserDetails = (url = ACCOUNT_API, api = axios) =>
  api({
    method: "get",
    url: `${url}/user`
  })

/**
 * Adds an Authorization header with the user's JWT to all subsequent requests.
 * @param {string} token The JWT token to add to outgoing requests.
 */
export const setAuthHeader = token => {
  /* eslint-disable dot-notation */
  axios.defaults.headers.common["Authorization"] = `JWT ${token}`
}

/**
 * Removes the Authorization header from all subsequent requests.
 */
export const removeAuthHeader = () => {
  /* eslint-disable dot-notation */
  axios.defaults.headers.common["Authorization"] = ""
}
