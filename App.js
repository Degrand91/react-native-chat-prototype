import React from "react"
import { StyleSheet, Text, View } from "react-native"
import { createStackNavigator } from "react-navigation"

import { Provider as StoreProvider } from "react-redux"
import store from "./App.store"

/** pages */
import Chat from "./components/Chat/Chat"
import Login from "./screens/Login"
import Signup from "./components/Login/Signup"

/** Navigation */
let RootStack = createStackNavigator({
  Login: { screen: Login }, //containers/views  goes here
  Signup: { screen: Signup },
  Chat: { screen: Chat }
})

let Navigation = createAppContainer(RootStack)

export default class App extends React.Component {
  render() {
    return (
      <StoreProvider store={store}>
        <Navigation />
      </StoreProvider>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    alignItems: "center",
    justifyContent: "center"
  }
})
